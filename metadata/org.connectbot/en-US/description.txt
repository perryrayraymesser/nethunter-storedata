ConnectBot is a powerful open-source Secure Shell (SSH) client. It can manage simultaneous SSH sessions, create secure tunnels, and copy/paste between other applications.

This client allows you to connect to Secure Shell servers that typically run on UNIX-based servers.

This app is built and signed by Kali NetHunter.
