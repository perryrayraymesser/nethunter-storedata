All you need is to launch an intent in the form "iqsrc://rtl_tcp_arguments" where you replace rtl_tcp_arguments with the arguments that you want your application to run rtl_tcp with and the magic will be done behind the scenes.

This driver could be used by third party applications to implement Software Defined Radio.

It does not require root for Android 3.1 and above.

An Android port of rtl-sdr's rtl_tcp and libhackrf

This app is released under GPL2+ as a derivative work of rtl_tcp and libhackrf. The source code can be found at https://github.com/martinmarinov/rtl_tcp_andro-

This driver could be used by third party applications to implement Software Defined Radio (like SDR Touch)

Keep in mind that this application on its own may not be very useful as it is just a driver. It is able to stream rtl_tcp over the network.

Supported rtl-sdr dongles:

- Any Generic RTL2832U
- DigitalNow Quad DVB-T PCI-E card
- Leadtek WinFast DTV Dongle mini D
- Genius TVGo DVB-T03 USB dongle (Ver. B)
- Terratec Cinergy T Stick Black (rev 1)
- Terratec NOXON DAB/DAB+ USB dongle (rev 1)
- Terratec Deutschlandradio DAB Stick
- Terratec NOXON DAB Stick - Radio Energy
- Terratec Media Broadcast DAB Stick
- Terratec BR DAB Stick
- Terratec WDR DAB Stick
- Terratec MuellerVerlag DAB Stick
- Terratec Fraunhofer DAB Stick
- Terratec Cinergy T Stick RC (Rev.3)
- Terratec T Stick PLUS
- Terratec NOXON DAB/DAB+ USB dongle (rev 2)
- PixelView PV-DT235U(RN)
- Astrometa DVB-T/DVB-T2
- Compro Videomate U620F
- Compro Videomate U650F
- Compro Videomate U680F
- GIGABYTE GT-U7300
- DIKOM USB-DVBT HD
- Peak 102569AGPK
- KWorld KW-UB450-T USB DVB-T Pico TV
- Zaapa ZT-MINDVBZP
- SVEON STV20 DVB-T USB & FM
- Twintech UT-40
- ASUS U3100MINI_PLUS_V2
- SVEON STV27 DVB-T USB & FM
- SVEON STV21 DVB-T USB & FM
- Dexatek DK DVB-T Dongle (Logilink VG0002A)
- Dexatek DK DVB-T Dongle (MSI DigiVox mini II V3.0)
- Dexatek Technology Ltd. DK 5217 DVB-T Dongle
- MSI DigiVox Micro HD
- Sweex DVB-T USB
- GTek T803
- Lifeview LV5TDeluxe
- MyGica TD312
- PROlectrix DV107669

Supported HackRF devices:

- HackRF One
- Rad1o
- HackRF Jawbreake
.
 
